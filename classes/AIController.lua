local registry = require "registry"
local vector = require "lib.hump.vector"

class "AIController"
{
	__init__ = function(self, player, playerMech, enemyMech)
		self.player = player
		self.playerMech = playerMech
		self.enemyMech = enemyMech
		self.enemyVel = vector(0, 0)
		self.lastEnemyPosition = self.enemyMech.pos
		self.timeTillEvaluate = 0
		self.desiredHeight = 0
		self.heightThreshold = 5
		self.desiredHorizontal = 0
		self.horizontalThreshold = 5
		self.matchHeight = true
		self.goInForKill = false
		self.timeSinceLastPunch = 0
		print("Cerberus AI Initiated")
	end,

	isDown = function(self, keyname)
		if keyname == "up" then
			return self:shouldJump()
		elseif keyname == "down" then
			return self:shouldDive()
		elseif keyname == "left" then
			return self:shouldMoveLeft()
		elseif keyname == "right" then
			return self:shouldMoveRight()
		elseif keyname == "hadouken" then
			return self:shouldHadouken()
		elseif keyname == "punch" then
			return self:shouldPunch()
		end

		return false
	end,
	
	shouldJump = function(self)
		if self.enemyMech.projectile 
		and ((self.enemyMech.projectile.pos.x < self.playerMech.pos.x + self.playerMech.size.x and self.enemyMech.projectile.vel.x > 0) 
		or (self.enemyMech.projectile.pos.x > self.playerMech.pos.x - self.playerMech.size.x and self.enemyMech.projectile.vel.x < 0)) then
			if self.enemyMech.projectile.pos.y + 5 > FLOORHEIGHT - self.playerMech.size.y then
				if self.playerMech.pos.y - self.enemyMech.projectile.pos.y > -25 then
					return true
				end
			end
		else
			--return self.enemyMech.pos.y < self.playerMech.pos.y and self.enemyMech.pos.y - self.playerMech.pos.y < -20
			if self.matchHeight then
				return self.playerMech.pos.y - self.enemyMech.pos.y > self.heightThreshold
			else
				return self.playerMech.pos.y - self.desiredHeight > self.heightThreshold
			end
		end
	end,
	
	shouldDive = function(self)
		if self.playerMech.pos.y >= FLOORHEIGHT + self.playerMech.imageOffset.y then
			return false
		end
	
		if self.enemyMech.projectile 
		and ((self.enemyMech.projectile.pos.x < self.playerMech.pos.x + self.playerMech.size.x and self.enemyMech.projectile.vel.x > 0) 
		or (self.enemyMech.projectile.pos.x > self.playerMech.pos.x - self.playerMech.size.x and self.enemyMech.projectile.vel.x < 0)) then
			if self.enemyMech.projectile.pos.y + 5 < FLOORHEIGHT - self.playerMech.size.y then
				if self.playerMech.pos.y - self.enemyMech.projectile.pos.y < 25 then
					return true
				end
			end
		else
			 --return self.enemyMech.pos.y > self.playerMech.pos.y and self.enemyMech.pos.y - self.playerMech.pos.y > 20
			if self.matchHeight then
				return self.enemyMech.pos.y - self.playerMech.pos.y > self.heightThreshold 
			else
				return self.desiredHeight - self.playerMech.pos.y > self.heightThreshold
			end
		end
	end,
	
	shouldMoveLeft = function(self)
		if self.enemyMech.pos.x - self.playerMech.pos.x < -5 and not self.playerMech.facingLeft then
			return true
		elseif self.goInForKill then
			return self.playerMech.pos.x > self.enemyMech.pos.x and self.playerMech.pos.x > self.enemyMech.pos.x + 20 - self.horizontalThreshold 
				or (self.playerMech.pos.x < self.enemyMech.pos.x and self.enemyMech.pos.x < self.playerMech.pos.x + 20 + self.horizontalThreshold)
		else
			return self.playerMech.pos.x - self.desiredHorizontal > self.horizontalThreshold 
		end 
	end,
	
	shouldMoveRight = function(self)
		if self.enemyMech.pos.x - self.playerMech.pos.x > 5 and self.playerMech.facingLeft then
			return true
		elseif self.goInForKill then
			return self.enemyMech.pos.x > self.playerMech.pos.x and self.enemyMech.pos.x > self.playerMech.pos.x + 20 - self.horizontalThreshold 
				or (self.enemyMech.pos.x < self.playerMech.pos.x and self.playerMech.pos.x < self.enemyMech.pos.x + 20 + self.horizontalThreshold)
		else
			return self.desiredHorizontal - self.playerMech.pos.x > self.horizontalThreshold 
		end
	end,
	
	shouldHadouken = function(self)
		if self.enemyMech.pos.y < self.playerMech.pos.y and self.enemyVel.y > 10 then
			if self.playerMech.pos.y - self.enemyMech.pos.y > 15 then
				self.timeSinceLastPunch = 0
				return true
			end
		elseif self.enemyMech.pos.y > self.playerMech.pos.y and self.enemyVel.y < -10 then
			if self.playerMech.pos.y - self.enemyMech.pos.y < -15 then
				self.timeSinceLastPunch = 0
				return true
			end
		elseif (self.playerMech.pos.x  - self.enemyMech.pos.x > 35 or self.playerMech.pos.x  - self.enemyMech.pos.x < -35)
		and self.playerMech.pos.y  - self.enemyMech.pos.y < 10 and self.playerMech.pos.y  - self.enemyMech.pos.y > -10 then
			if love.math.random(0,1) == 1 and self.timeSinceLastPunch > 1.5 and self.goInForKill == false then
				self:killMode()
			end
			return true
		end
		
	end,
	
	shouldPunch = function(self)
		if self.playerMech.pos.y  - self.enemyMech.pos.y < 20 and self.playerMech.pos.y  - self.enemyMech.pos.y > -20 then
			if self.playerMech.pos.x  - self.enemyMech.pos.x < 25 and self.playerMech.pos.x  - self.enemyMech.pos.x > -25 then
				if love.math.random(0,1) == 1 and  self.matchHeight then
					self:holdGround()
					self.timeTillEvaluate = 1
				end
				self.timeSinceLastPunch = 0
				return true
			else
				if self.timeSinceLastPunch > 1.5 and self.goInForKill == false then
					self:killMode()
				end
				return false
			end
		end
	end,
	
	update = function(self, dt)
		self.enemyVel = (self.enemyMech.pos - self.lastEnemyPosition)/dt
		self.lastEnemyPosition = self.enemyMech.pos
		
		self.timeTillEvaluate = self.timeTillEvaluate - dt
		self.timeSinceLastPunch = self.timeSinceLastPunch + dt
		if(self.timeTillEvaluate < 0) then
			self.timeTillEvaluate = love.math.random(1,2) + love.math.random(1,2)
			local randomState = love.math.random(0,10)
			if(randomState > 6) then
				self:killMode()
			elseif(randomState > 3) then
				print("Track Height")
				self.matchHeight = true
				self.heightThreshold = love.math.random(5,20)
				
				self.goInForKill = false
				self.desiredHorizontal = love.math.random(7,87) + love.math.random(7,100) -- 2 rands to give higher weighting to centre of screen
				self.horizontalThreshold = love.math.random(5,20)
			else
				self:holdGround()
			end
		end
		
	end,
	
	killMode = function(self)
		print("Ooooh yeah, kill mode engaged!")
		print("Time to kick ass and chew bubble gum, but I'm all under water.")
		self.goInForKill = true
		self.heightThreshold = 5
		self.matchHeight = true
		self.horizontalThreshold = 5
	end,
	
	holdGround = function(self)
		print("Hold Ground")
		print("MIIIINE")
		self.matchHeight = false
		self.desiredHeight = love.math.random(25,150) 
		self.heightThreshold = love.math.random(5,20)
		
		self.goInForKill = false
		self.desiredHorizontal = love.math.random(7,87) + love.math.random(7,100) -- 2 rands to give higher weighting to centre of screen
		self.horizontalThreshold = love.math.random(5,20)
	end,

	hasJoystick = function(self)
		return true -- Always be ineligible for joystick attachment
	end,
}
