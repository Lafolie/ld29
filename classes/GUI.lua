local vector = require "lib.hump.vector"

class "GUI"
{ 

	__init__ = function(self,mech1,mech2)
		self.mech1 = mech1
		self.mech2 = mech2
		self.winPin = love.graphics.newImage("gfx/gui/winPin.png")
		
		self.player1HealthbarPosition = vector(10,5)
		self.hpFrame1 = love.graphics.newImage("gfx/gui/hpFrame_Player.png")

		self.player2HealthbarPosition = vector(115,5)
		self.hpFrame2 = love.graphics.newImage("gfx/gui/hpFrame_Enemy.png")

	end,

	draw = function(self)
		self:drawPlayer1Health()
		self:drawPlayer2Health()
	end,
	
	drawPlayer1Health = function(self)
		local lifeperc = self.mech1.hp/1000
		local r, g = (1-lifeperc*lifeperc)*255, (1-(1-lifeperc)*(1-lifeperc))*255
		local xpos = self.player1HealthbarPosition.x
		local imgOff = 5

		love.graphics.setColor(50, 50, 50)
		love.graphics.rectangle("fill", self.player1HealthbarPosition.x, self.player1HealthbarPosition.y, 75, 5)
		love.graphics.setColor(r, g, 0)
		love.graphics.rectangle("fill", xpos, self.player1HealthbarPosition.y, self.mech1.hp/1000*75, 5)
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.hpFrame1, self.player1HealthbarPosition.x-imgOff, self.player1HealthbarPosition.y-2)

		for i = 1, self.mech1.wins do
			local startx = self.player1HealthbarPosition.x
			local dist = 9
			startx = startx + 80
			dist = -dist

			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.draw(self.winPin, startx + dist * i, self.player1HealthbarPosition.y + 7)
		end
	end,
	
	drawPlayer2Health = function(self)
		local lifeperc = self.mech2.hp/1000
		local r, g = (1-lifeperc*lifeperc)*255, (1-(1-lifeperc)*(1-lifeperc))*255
		local xpos = self.player2HealthbarPosition.x
		local imgOff = 5
		xpos = xpos + (75 - self.mech2.hp/1000*75)
		imgOff = 2

		love.graphics.setColor(50, 50, 50)
		love.graphics.rectangle("fill", self.player2HealthbarPosition.x, self.player2HealthbarPosition.y, 75, 5)
		love.graphics.setColor(r, g, 0)
		love.graphics.rectangle("fill", xpos, self.player2HealthbarPosition.y, self.mech2.hp/1000*75, 5)
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.hpFrame2, self.player2HealthbarPosition.x-imgOff, self.player2HealthbarPosition.y-2)

		for i = 1, self.mech2.wins do
			local startx = self.player2HealthbarPosition.x
			local dist = 9
			startx = startx - 5

			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.draw(self.winPin, startx + dist * i, self.player2HealthbarPosition.y + 7)
		end
	end,
		
}