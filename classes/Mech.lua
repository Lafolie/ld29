require "classes.Entity"
require "classes.Drawable"
require "classes.Actor"
require "classes.Living"
require "classes.Shader"
require "classes.Controller"
require "classes.Projectile"
require "classes.Bubble"
require "classes.Timer"

local vector = require "lib.hump.vector"
local registry = require "registry"
local snd = require "soundmanager"

local function drawBbox(shape)
	local x1, y1, x2, y2 = shape:bbox()
	return love.graphics.rectangle("fill", x1, y1, x2-x1, y2-y1)
end

local function randColor(saturated)
	local genSingleColor
	if saturated then
		function genSingleColor()
			return (love.math.random() + 1)/2
		end
	else
		function genSingleColor()
			return love.math.random()
		end
	end
	local r = genSingleColor()
	local g = genSingleColor()
	local b = genSingleColor()
	return r, g, b
end

local function sign(x)
	if x == 0 then return 0 end
	if x > 0 then return 1 else return -1 end
end

class "Mech" (Entity, Drawable, Actor, Living)
{
	__init__ = function(self, x, y, player,faceLeft)
		Entity.__init__(self)
		self.player = player
		self.pos = vector(x, y)
		self.gravity = 0
		self.hp = 1000
		self.wins = 0
		self.facingLeft = faceLeft
		self.controller = Controller(player)
		self.animtimer = 0
		self.projectile = nil
		self.bubbles = {}
		self.bubbletimer = Timer("periodic", 0.025, function()
			if self.controller:isDown("up") or self.controller:isDown("down") then
				local xoff = self.facingLeft and 9 or -12
				table.insert(self.bubbles, Bubble(self.pos.x+xoff, self.pos.y+2))
			end
		end)

		self.canAttack = false
		self.attackTimer = Timer("once", 1.5, function()
			self.canAttack = true
		end)
		
		self.images = {}
		local model = {"Titan", "Colossus", "Leviathan"}
		local bodyModel, armAttachmentDamnYouDale, backModel

		for i, v in ipairs{"back", "body", "leg", "arm"} do
			local model = model[love.math.random(#model)]
			local prefix = v:upper()
			if v == "body" then bodyModel = model end
			if v == "back" then backModel = model end
			if v == "arm" then armAttachmentDamnYouDale = model end
			if v == "leg" then model = backModel end
			self.images[v] = love.graphics.newImage("gfx/mech/" .. prefix .. "_" .. model .. ".png")
			self.images[v .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. prefix .. "_" .. model .. ".png")
		end

		self.images.subMask = love.graphics.newImage("gfx/mech/M_SUB_" .. bodyModel .. ".png")
		self.shader = Shader("texAtlas.glsl", "mechPaint.glsl")
		self.size = vector(32, 32)
		self.imageOffset = -self.size/2

		self.shader.uniforms.mask_sub = self.images.subMask
		self.shader.uniforms.cel_size =
			{
				self.size.x/self.images.subMask:getWidth(),
				self.size.y/self.images.subMask:getHeight(),
				1,
				1
			}
		self.shader.uniforms.current_cel = {1, 2}
		self.shader.uniforms.user_color = {randColor()}
		self.shader.uniforms.user_color_sub = {randColor(true)}

		if self.player == 1 then
			if PLAYER1COLOUR then
				self.shader.uniforms.user_color = PLAYER1COLOUR
			end
			if PLAYER1SUBCOLOUR then
				self.shader.uniforms.user_color_sub = PLAYER1SUBCOLOUR
			end
			if PLAYER1BODY then
				self.images["body"] = love.graphics.newImage("gfx/mech/" .. "BODY" .. "_" .. PLAYER1BODY .. ".png")
				self.images["body" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "BODY" .. "_" .. PLAYER1BODY .. ".png")
				self.images.subMask = love.graphics.newImage("gfx/mech/M_SUB_" .. PLAYER1BODY .. ".png")
				self.shader.uniforms.mask_sub = self.images.subMask
			end
			if PLAYER1ARMS then
				self.images["arm"] = love.graphics.newImage("gfx/mech/" .. "ARM" .. "_" .. PLAYER1ARMS .. ".png")
				self.images["arm" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "ARM" .. "_" .. PLAYER1ARMS .. ".png")
				armAttachmentDamnYouDale = PLAYER1ARMS
			end
			if PLAYER1LEGS then
				self.images["leg"] = love.graphics.newImage("gfx/mech/" .. "LEG" .. "_" .. PLAYER1LEGS .. ".png")
				self.images["leg" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "LEG" .. "_" .. PLAYER1LEGS .. ".png")
				self.images["back"] = love.graphics.newImage("gfx/mech/" .. "BACK" .. "_" .. PLAYER1LEGS .. ".png")
				self.images["back" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "BACK" .. "_" .. PLAYER1LEGS .. ".png")
			end
		elseif FUKKENAI == false and self.player == 2 then
			if PLAYER2COLOUR then
				self.shader.uniforms.user_color = PLAYER2COLOUR
			end
			if PLAYER2SUBCOLOUR then
				self.shader.uniforms.user_color_sub = PLAYER2SUBCOLOUR
			end
			if PLAYER2BODY then
				self.images["body"] = love.graphics.newImage("gfx/mech/" .. "BODY" .. "_" .. PLAYER2BODY .. ".png")
				self.images["body" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "BODY" .. "_" .. PLAYER2BODY .. ".png")
				self.images.subMask = love.graphics.newImage("gfx/mech/M_SUB_" .. PLAYER2BODY .. ".png")
				self.shader.uniforms.mask_sub = self.images.subMask
			end
			if PLAYER2ARMS then
				self.images["arm"] = love.graphics.newImage("gfx/mech/" .. "ARM" .. "_" .. PLAYER2ARMS .. ".png")
				self.images["arm" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "ARM" .. "_" .. PLAYER2ARMS .. ".png")
				armAttachmentDamnYouDale = PLAYER2ARMS
			end
			if PLAYER2LEGS then
				self.images["leg"] = love.graphics.newImage("gfx/mech/" .. "LEG" .. "_" .. PLAYER2LEGS .. ".png")
				self.images["leg" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "LEG" .. "_" .. PLAYER2LEGS .. ".png")
				self.images["back"] = love.graphics.newImage("gfx/mech/" .. "BACK" .. "_" .. PLAYER2LEGS .. ".png")
				self.images["back" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "BACK" .. "_" .. PLAYER2LEGS .. ".png")
			end
		end

		self.bodyOffset = vector(-3, -5)
		self.armOffset = vector(8, 0)
		self.fistOffset = vector(12, 0)
		
		if self.facingLeft then
			self.bodyOffset.x = - self.bodyOffset.x
			self.armOffset.x = - self.armOffset.x 
			self.fistOffset.x = - self.fistOffset.x
		end

		self.body = self.pos + self.bodyOffset
		self.arm = self.pos + self.armOffset 
		self.fist = self.pos + self.fistOffset

		self.body = HCShapes.newRectangleShape(self.body.x, self.body.y, 20, 19)
		self.arm = HCShapes.newRectangleShape(self.arm.x, self.arm.y, 6, 7)
		self.fist = HCShapes.newRectangleShape(self.fist.x, self.fist.y, 6, 7)
		
		self.damagingShapes = {}

		registry.shapes.register(self, self.body)
		registry.shapes.register(self, self.arm)
		registry.shapes.register(self, self.fist)

		self.punching = false
		self.punchTimer = Timer("once", 5/8, function()
			self.punching = false
			self.damagingShapes[self.fist] = false
		end)
		self.wineyDaleTimer = Timer("once", 1/8, function()
			if self.punching then
				self.damagingShapes[self.fist] = { damage = 100, stuns = false, singleHit = true }
				if armAttachmentDamnYouDale == "Titan" then
					snd.playRand(sfx.drill)
				elseif armAttachmentDamnYouDale == "Colossus" then
					snd.playRand(sfx.attackSwoosh)
				elseif armAttachmentDamnYouDale == "Leviathan" then
					snd.playRand(sfx.sword)
				end
			end
		end)

		self.lastXVel = 0
	end,

	update = function(self, dt)
		self.controller:update(dt)
		self.attackTimer:update(dt)
		self.punchTimer:update(dt)
		self.wineyDaleTimer:update(dt)

		local movement = vector(0, 0)
		if self.pos.y < FLOORHEIGHT+self.imageOffset.y then
			-- gravitay
			self.gravity = self.gravity + 5 * dt
			movement.y = self.gravity
		else
			self.gravity = 0
		end

		self.bubbletimer:update(dt)
		if not self.punching then
			if self.controller:isDown("up") then
				movement.y = movement.y - 50
				self.gravity = 0
			end
			if self.controller:isDown("down") then
				movement.y = movement.y + 50
			end
			if self.controller:isDown("left") then
				movement.x = movement.x - 50
				if not self.facingLeft and self.enemyDirection == 'left' then
					self:flipFacing()
				end
			end
			if self.controller:isDown("right") then
				movement.x = movement.x + 50
				if self.facingLeft  and self.enemyDirection == 'right' then
					self:flipFacing()
				end
			end
		end

		if self.controller:isDown("punch") and self.canAttack then
			self:punch(dt)
		end

		if self.projectile and self.projectile:dead() then
			self.projectile = nil
		end

		if self.controller:isDown("hadouken") and not self.projectile and self.canAttack then
			local dir = self.facingLeft and -1 or 1
			self.projectile = Projectile(self.pos.x + dir * 25, self.pos.y, dir * 70, 0)
			self.canAttack = false
			self.attackTimer:reset()
			snd.playRand(sfx.torpedoFire)
		end

		if movement.x ~= 0 or movement.y ~= 0 then
			self.updateHitBoxes = true
		end

		if self.updateHitBoxes then
			self.pos = self.pos + movement*dt

			if self.pos.y > FLOORHEIGHT+self.imageOffset.y then
				self.pos.y = FLOORHEIGHT+self.imageOffset.y
			elseif self.pos.y < 14 then
				self.pos.y = 14
			end
			
			if self.pos.x < 13 then
				self.pos.x = 13
			elseif self.pos.x > 187 then
				self.pos.x = 187
			end

			self.body:moveTo((self.pos + self.bodyOffset):unpack())
			self.arm:moveTo((self.pos + self.armOffset):unpack())
			self.fist:moveTo((self.pos + self.fistOffset):unpack())

			self.updateHitBoxes = false
		end

		if self.punching then
			self.animtimer = self.animtimer + dt
			if self.animtimer >= 0.125 then
				local nextframe = self.shader.uniforms.current_cel[1]%5+1
				self.animtimer = self.animtimer - 0.125
				self.shader.uniforms.current_cel = {nextframe, 3}
			end
		elseif movement.x ~= 0 then
			self.animtimer = self.animtimer + dt
			if self.animtimer >= 0.125 then
				local nextframe = self.shader.uniforms.current_cel[1]
				if (movement.x > 0) == self.facingLeft then
					nextframe = (nextframe-2)%6+1
				else
					nextframe = nextframe%6+1
				end

				if movement.y ~= 0 then
					nextframe = 3
				end

				self.animtimer = self.animtimer - 0.125
				self.shader.uniforms.current_cel = {nextframe, 2}
			end
		elseif movement.y ~= 0 then
			self.shader.uniforms.current_cel = {4, 2}
		else
			self.shader.uniforms.current_cel = {1, 1}
		end

		for i, v in pairs(self.bubbles) do
			if v:dead() then self.bubbles[i] = nil end
		end

		if sign(self.lastXVel) ~= sign(movement.x) then
			if love.math.random() > 0.6 then
				snd.playRand(sfx.mech)
			end
		end
		self.lastXVel = movement.x
	end,
	
	punch =  function(self, dt)
		self.attackTimer:reset()
		self.canAttack = false
		self.punching = true
		self.punchTimer:reset()
		self.wineyDaleTimer:reset()
		self.shader.uniforms.current_cel[1] = 0
	end,
	
	flipFacing = function(self)
		self.facingLeft = not self.facingLeft
		self.bodyOffset.x = - self.bodyOffset.x
		self.armOffset.x = - self.armOffset.x 
		self.fistOffset.x = - self.fistOffset.x
	end,

	collideWith = function(self, shape, other, dx, dy)
		if self.damagingShapes[shape] and class.isinstance(other, Living) then
			other:damage(self.damagingShapes[shape].damage, {knockback = true})
			if self.damagingShapes[shape].singleHit then
				self.damagingShapes[shape] = false
			end
			
		end
	end,
	
	damage = function(self, amount, extra)
		self.hp = self.hp - amount
		snd.playRand(sfx.hit)

		if extra and extra.knockback then
			local amount = 10
			if self.enemyDirection == "right" then
				amount = -amount
			end

			self.pos.x = self.pos.x + amount
			self.updateHitBoxes = true
		end

		if self.hp < 0 then
			self.hp = 0
			print("Babooooom bitches")
		end
	end,

	draw = function(self)
		love.graphics.setColor(255, 255, 255)
		self.shader:apply()

		-- back, body, leg, arm
		local imgPos = self.pos + self.imageOffset
		if self.facingLeft then
			imgPos.x = imgPos.x - 2*self.imageOffset.x
		end
		for i, v in ipairs{"back", "body", "leg", "arm"} do
			self.shader.uniforms.mask = self.images[v .. "Mask"]
			love.graphics.draw(self.images[v], imgPos.x, imgPos.y, 0, self.facingLeft and -1 or 1, 1)
		end

		self.shader:unapply()
		--[[love.graphics.setColor(255, 255, 255, 50)
		drawBbox(self.body)
		drawBbox(self.arm)
		drawBbox(self.fist)]]

	end,
}
