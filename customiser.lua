local customiser = {}

local vector = require "lib.hump.vector"
local snd = require "soundmanager"

local shader, images, size, imageOffset, pos, selection

local xOff, yOff, xTarget, yTarget, t
local r, g, b, rTarget, gTarget, bTarget
local bg1, bg2, bgAlpha, bgFade



local menudefinitions = 
{
	{name = "Body", value = "Titan"},
	{name = "Arms", value = "Titan"},
	{name = "Legs", value = "Titan"},
	{name = "R", value = 120},
	{name = "G", value = 120},
	{name = "B", value = 120},
	{name = "R", value = 110},
	{name = "Secondary G", value = 220},
	{name = "B", value = 115},
	{name = "", value = "OK", callback = function() if not FUKKENAI and SELECTION == 1 then  switchState("customiser") SELECTION = 2 else snd.stopMusic()
			MENUMUSICPLAYING = false switchState("game") end end},
}




local function randColor()
	local function genSingleColor()
		return (love.math.random() + love.math.random() + love.math.random())/3
	end
	local r = genSingleColor()
	local g = genSingleColor()
	local b = genSingleColor()
	return r, g, b
end

local function setModel(model,part)
	if part  == "Body" then
		images["body"] = love.graphics.newImage("gfx/mech/" .. "BODY" .. "_" .. model .. ".png")
		images["body" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "BODY" .. "_" .. model .. ".png")
		images.subMask = love.graphics.newImage("gfx/mech/M_SUB_" .. model .. ".png")
		shader.uniforms.mask_sub = images.subMask
	elseif part == "Arms" then
		images["arm"] = love.graphics.newImage("gfx/mech/" .. "ARM" .. "_" .. model .. ".png")
		images["arm" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "ARM" .. "_" .. model .. ".png")
	elseif part == "Legs" then
		images["leg"] = love.graphics.newImage("gfx/mech/" .. "LEG" .. "_" .. model .. ".png")
		images["leg" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "LEG" .. "_" .. model .. ".png")
		images["back"] = love.graphics.newImage("gfx/mech/" .. "BACK" .. "_" .. model .. ".png")
		images["back" .. "Mask"] = love.graphics.newImage("gfx/mech/M_" .. "BACK" .. "_" .. model .. ".png")
	end	
end

local function setColours()
	shader.uniforms.user_color = {menudefinitions[4].value/255,menudefinitions[5].value/255,menudefinitions[6].value/255}
	shader.uniforms.user_color_sub = {menudefinitions[7].value/255,menudefinitions[8].value/255,menudefinitions[9].value/255}
end

function customiser.load()
	
	smallerFont = love.graphics.newFont("font/PressStart2P.ttf", 8)
	smallerFont:setFilter("nearest", "nearest")

	for i=1,3,1 do
		menudefinitions[i].leftCallback = function() if menudefinitions[i].value == "Titan" then menudefinitions[i].value = "Colossus" elseif menudefinitions[i].value == "Colossus"  then menudefinitions[i].value = "Leviathan"  else menudefinitions[i].value = "Titan" end 
			setModel(menudefinitions[i].value,menudefinitions[i].name) end
		menudefinitions[i].rightCallback = function() if menudefinitions[i].value == "Titan" then menudefinitions[i].value = "Leviathan" elseif menudefinitions[i].value == "Colossus"  then menudefinitions[i].value = "Titan"  else menudefinitions[i].value = "Colossus" end  
			setModel(menudefinitions[i].value,menudefinitions[i].name) end
			
		menudefinitions[i].value = "Titan"
		
	end
	
	for i=4,9,1 do
		menudefinitions[i].leftCallback = function() if menudefinitions[i].value > 0 then menudefinitions[i].value = menudefinitions[i].value -5 end setColours() end
		menudefinitions[i].rightCallback = function() if menudefinitions[i].value < 255 then menudefinitions[i].value = menudefinitions[i].value +5 end setColours() end
	end
	
	menudefinitions[4].value = 120
	menudefinitions[5].value = 120
	menudefinitions[6].value = 120
	menudefinitions[7].value = 110
	menudefinitions[8].value = 220
	menudefinitions[9].value = 115
	
	selection = 1
	pos = vector(30, 60)
	images = {}

	shader = Shader("texAtlas.glsl", "mechPaint.glsl")
	size = vector(32, 32)
	imageOffset = -size/2

	for i=1,3,1 do
		setModel(menudefinitions[i].value,menudefinitions[i].name)
	end

	shader.uniforms.cel_size =
	{
		size.x/images.subMask:getWidth(),
		size.y/images.subMask:getHeight(),
		1,
		1
	}
	shader.uniforms.current_cel = {1, 2}
	setColours()

	xOff, yOff = 0, 0
	xTarget, yTarget = 0, 0
	r, g, b = 255, 255, 255
	rTarget, gTarget, bTarget = 255, 255, 255

	bg1 = love.graphics.newImage("gfx/gui/menu01.png")
	bg2 = love.graphics.newImage("gfx/gui/menu02.png")
	currentBg = bg1
	bgAlpha = 0
	bgFade = 50

	t = Timer("periodic", 0.050, function()
		xTarget, yTarget = love.math.random()*6-3, love.math.random()*6-3

		rTarget, gTarget, bTarget = love.math.random(0, 255), love.math.random(0, 255), love.math.random(0, 255)

		bgAlpha = 0
		bgFade = 50
	end)
end

function customiser.unload()
	if SELECTION == 1 then
		PLAYER1COLOUR = {unpack(shader.uniforms.user_color)}
		PLAYER1SUBCOLOUR ={unpack(shader.uniforms.user_color_sub)}
		PLAYER1BODY = menudefinitions[1].value
		PLAYER1ARMS = menudefinitions[2].value
		PLAYER1LEGS = menudefinitions[3].value
	elseif SELECTION == 2 then
		PLAYER2COLOUR = {unpack(shader.uniforms.user_color)}
		PLAYER2SUBCOLOUR ={unpack(shader.uniforms.user_color_sub)}
		PLAYER2BODY = menudefinitions[1].value
		PLAYER2ARMS = menudefinitions[2].value
		PLAYER2LEGS = menudefinitions[3].value
	end
end

function customiser.update(dt)
	t:update(dt)
	xOff = xOff - (xOff-xTarget)*dt
	yOff = yOff - (yOff-yTarget)*dt
	r = r - (r-rTarget)*dt
	g = g - (g-gTarget)*dt
	b = b - (b-bTarget)*dt

	bgAlpha = bgAlpha + bgFade * dt
	if bgAlpha <= 0 then
		bgAlpha = 0
		bgFade = 50
	elseif bgAlpha >= 200 then
		bgAlpha = 200
		bgFade = -50
	end

end

function customiser.draw()
	love.graphics.scale(4, 4)

	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(bg1)
	love.graphics.setColor(255, 255, 255, bgAlpha)
	love.graphics.draw(bg2)

	love.graphics.setColor(255, 255, 255)
	shader:apply()

	-- back, body, leg, arm
	local imgPos = pos + imageOffset

	for i, v in ipairs{"back", "body", "leg", "arm"} do
		shader.uniforms.mask = images[v .. "Mask"]
		love.graphics.draw(images[v], imgPos.x, imgPos.y)
	end

	shader:unapply()

	love.graphics.setColor(r, g, b)
	love.graphics.printf("BESTU GEMU ROBOTO FIGHTERU", xOff, yOff , 200, "center")
	love.graphics.printf("PLAYER "..tostring(SELECTION), xOff, yOff + 30, 200, "center")
	
	love.graphics.setFont(smallerFont)
	
	for i, v in ipairs(menudefinitions) do
		local str = tostring(v.value)
		if selection == i then
			str = "> " .. str .. " <"
		end
		if i > 3 then
			love.graphics.printf(v.name, xOff - 40, yOff + 40 + 10 * i, 150, "right")
		else 
			love.graphics.printf(v.name, xOff - 30, yOff + 40 + 10 * i, 200, "center")
		end
		love.graphics.printf(str, xOff + 50, yOff + 40 + 10 * i, 200, "center")
	end
	
	love.graphics.printf("Primary", xOff - 15, yOff + 90, 150, "center")
	
	love.graphics.setFont(timerFont)
end

function customiser.keypressed(key)
	if key == "down" or key == "s" then
		selection = selection%#menudefinitions + 1
	elseif key == "up" or key == "w" then
		selection = (selection-2)%#menudefinitions+1
	elseif key == "return" then
		local chosen = menudefinitions[selection]
		if chosen.callback then
			chosen.callback()
		end
	elseif key == "left" or key == "a" then
		local chosen = menudefinitions[selection]
		if chosen.leftCallback then
			chosen.leftCallback()
		end
	elseif key == "right" or key == "d" then
		local chosen = menudefinitions[selection]
		if chosen.rightCallback then
			chosen.rightCallback()
		end
	end
end

function customiser.joystickpressed(joystick, button)
end

return customiser
