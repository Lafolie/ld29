class = require "lib.slither" -- BOO, globals
local game = require "game"
local menu = require "menu"

local customiser = require "customiser"
local victory = require "victory"
local soundmanager = require "soundmanager"

HCShapes = require "lib.HardonCollider.shapes"
function HCShapes.newRectangleShape(x, y, w, h)
	return HCShapes.newPolygonShape(x, y, x+w, y, x+w, y+h, x, y+h)
end

local state = menu

function switchState(name)
	if state.unload then
		state.unload()
	end

	if name == "game" then
		state = game
	elseif name == "menu" then
		state = menu
	elseif name == "customiser" then
		state = customiser
	elseif name == "victory" then
		state = victory
	end

	state.load()
end

function love.load()
	love.graphics.setDefaultFilter("nearest", "nearest")
	timerFont = love.graphics.newFont("font/BMarmy.TTF", 12)
	timerFont:setFilter("nearest", "nearest")
	love.graphics.setFont(timerFont)

	state.load()
end

function love.update(dt)
	soundmanager.update()
	state.update(dt)
end

function love.draw()
	state.draw()
end

function love.keypressed(key)
	if key == "escape" then
		return switchState("menu")
	end

	if state.keypressed then
		state.keypressed(key)
	end
end

function love.joystickpressed(joystick, button)
	if state.joystickpressed then
		state.joystickpressed(joystick, button)
	end
end

function print(...)
	local t = {...}
	for i, v in ipairs(t) do
		t[i] = tostring(v)
	end

	local str = table.concat(t, "\t")
	return io.write("[" .. os.date() .. "] " .. str .. "\n")
end
