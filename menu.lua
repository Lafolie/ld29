require "classes.Timer"

local snd = require "soundmanager"

local menu = {}

local menudefinitions = {
	["main"] =
	{
		{text = "1 Player", callback = function() FUKKENAI=true SELECTION = 1 switchState("customiser") end},
		{text = "2 Player", callback = function() FUKKENAI=false SELECTION = 1 switchState("customiser") end},
		{text = "Quit", callback = love.event.quit},
	},

	["useless"] =
	{
		{text = "Kawaii", callback = function() switchState("customiser") end},
		{text = "Backu", menu = "main"},
	},
}

local curmenu

local selection = 1
local xOff, yOff, xTarget, yTarget, t
local r, g, b, rTarget, gTarget, bTarget
local bg1, bg2, bgAlpha, bgFade

function menu.load()
	xOff, yOff = 0, 0
	xTarget, yTarget = 0, 0
	r, g, b = 255, 255, 255
	rTarget, gTarget, bTarget = 255, 255, 255

	t = Timer("periodic", 0.050, function()
		xTarget, yTarget = love.math.random()*6-3, love.math.random()*6-3

		rTarget, gTarget, bTarget = love.math.random(0, 255), love.math.random(0, 255), love.math.random(0, 255)
	end)

	curmenu = menudefinitions.main
	bg1 = love.graphics.newImage("gfx/gui/menu01.png")
	bg2 = love.graphics.newImage("gfx/gui/menu02.png")
	currentBg = bg1
	bgAlpha = 0
	bgFade = 50
	
	if not MENUMUSICPLAYING then
		snd.playMusic("bgm/menuMusic.mp3")
		MENUMUSICPLAYING =true
	end
end

function menu.update(dt)
	t:update(dt)
	xOff = xOff - (xOff-xTarget)*dt
	
	yOff = yOff - (yOff-yTarget)*dt
	r = r - (r-rTarget)*dt
	g = g - (g-gTarget)*dt
	b = b - (b-bTarget)*dt

	bgAlpha = bgAlpha + bgFade * dt
	if bgAlpha <= 0 then
		bgAlpha = 0
		bgFade = 50
	elseif bgAlpha >= 200 then
		bgAlpha = 200
		bgFade = -50
	end
end

function menu.draw()
	love.graphics.scale(4, 4)
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(bg1)
	love.graphics.setColor(255, 255, 255, bgAlpha)
	love.graphics.draw(bg2)
	love.graphics.setColor(r, g, b)
	love.graphics.printf("UNDERWATER MECH\nFIGHTER", xOff, yOff + 20, 200, "center")
	for i, v in ipairs(curmenu) do
		local str = v.text
		if selection == i then
			str = "> " .. str .. " <"
		end

		love.graphics.printf(str, xOff, yOff + 50 + 15 * i, 200, "center")
	end
end

function menu.keypressed(key)
	if key == "down" or key == "s" then
		selection = selection%#curmenu + 1
	elseif key == "up" or key == "w" then
		selection = (selection-2)%#curmenu+1
	elseif key == "return" then
		local chosen = curmenu[selection]
		if chosen.callback then
			chosen.callback()
		elseif chosen.menu then
			curmenu = menudefinitions[chosen.menu]
			selection = 1
		end
	end
end

function menu.joystickpressed(joystick, button)
end

return menu
