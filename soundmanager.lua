local manager = {}

local sources = {}
local musicks = {}
local curMusak

function manager.play(file)
	local source = love.audio.newSource(file)
	source:play()

	table.insert(sources, source)
end

function manager.playRand(files)
	local source = files[love.math.random(#files)]
	source = love.audio.newSource(source)

	local pitch = love.math.random()*0.4+0.8
	source:setPitch(pitch)
	source:play()

	table.insert(sources, source)
end

function manager.update()
	for i, v in pairs(sources) do
		if not v:isPlaying() then
			sources[i] = nil
		end
	end

	if curMusak and not curMusak:isPlaying() then
		curMusak:rewind()
		curMusak:play()
	end
end

function manager.playMusic(...)
	musicks = {...}
	curMusak = musicks[love.math.random(#musicks)]
	curMusak = love.audio.newSource(curMusak)
	curMusak:setVolume(0.6)
	curMusak:play()
end

function manager.stopMusic()
	if curMusak then
		curMusak:stop()
	end

	curMusak = nil
	musicks = {}
end

return manager
