require "classes.Timer"
local snd = require "soundmanager"

local victory = {}

local t

function victory.load()
	snd.playRand(sfx.victory)
	t = Timer("once", 5, function()
		switchState("menu")
	end)
end

function victory.update(dt)
	t:update(dt)
end

function victory.draw()
	love.graphics.scale(4, 4)
	love.graphics.printf("PLAYER " .. AWINNERISYOU, 0, 30, 200, "center")
	love.graphics.printf("VICTORY ACHIEVED", 0, 50, 200, "center")
	love.graphics.printf("Or maybe you lost...", 0, 70, 200, "center")
end

return victory
